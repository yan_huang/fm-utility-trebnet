#!/usr/bin/env python

import getopt
import logging
import logging.handlers
import MySQLdb
import MySQLdb.cursors
import smtplib
import sys

# python 2.4 vs 2.6
try:
        from email.mime.multipart import MIMEMultipart
except:
        from email.MIMEMultipart import MIMEMultipart

# python 2.4 vs 2.6
try:
        from email.mime.text import MIMEText
except:
        from email.MIMEText import MIMEText

# Vars
#DEFAULT_REPORT_ADMIN        = 'dylan@electricmail.com'  # testing
DEFAULT_REPORT_ADMIN        = 'iahmad@trebnet.com'  # Imran Ahmad <iahmad@trebnet.com>
DEFAULT_EMAIL_SENDER        = 'Webmaster Mail Admin <webmaster@trebnet.com>'
DEFAULT_EMAIL_SERVER        = 'outbound.electric.net'
DEFAULT_EMAIL_PORT          = 25
DEFAULT_EMAIL_SUBJECT       = 'Webmaster Mail Delivery Failure Report'

DB_HOST = 'dbo'
DB_PORT = 3306
DB_USER = 'system'
DB_PASS = 'tjmwauki'
DB_NAME = 'bounce_aggr'

# Setup
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# Classes
    
# Subs
def usage(sender, maildir, parts):

    print """
Usage: %s --script=<scriptname> --maxlocks=<int>]

    -y, --yes                          : actually sends the email.
    -D, --Debug                        : enable Debug logging.
    -h, --help                         : this text.
    """ % (sys.argv[0], sender, maildir)

    return True

def text_message(list):
    return """In order to ensure that Property Match emails are successfully reaching the Prospects that you have added to TorontoMLS, please make certain that you have entered their email addresses correctly.

Delivery to the Webmaster/email address(es) below failed. Please verify and update the address(es) accordingly on TorontoMLS:

%s

This message was created automatically by the TREB email delivery system. Please do not reply. If you have any questions please contact TREB Helpdesk at 416-443-8111.
""" % '\n'.join(list)

def html_message(list):
    return """In order to ensure that Property Match emails are successfully reaching the Prospects that you have added to TorontoMLS, please make certain that you have entered their email addresses correctly.<br />
<br />
Delivery to the Webmaster/email address(es) below failed. Please verify and update the address(es) accordingly on TorontoMLS:<br />
<br />
<pre>
%s
</pre>
<br />
This message was created automatically by the TREB email delivery system. Please do not reply. If you have any questions please contact TREB Helpdesk at 416-443-8111.
""" % '\n'.join(list)

def autodelete_reported(db):
    logging.debug("deleting from the database records over 14 days old which have been reported.")

    # setup vars
    result  = True

    # create a cursor
    cursor = db.cursor()

    # query
    query = "DELETE FROM bounce_aggr WHERE mailbox = 'webmaster@trebnet.com' AND reported < DATE_SUB(now(),INTERVAL 14 day)"

    try:
        cursor.execute(query, ())
    except MySQLdb.IntegrityError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        result = False
    except MySQLdb.ProgrammingError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        result = False
    except MySQLdb.OperationalError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        result = False

    return result

def set_reported(db, row):
    logging.debug("setting repoted for: [%s :: %s]" % (row['filename'], row['recipient']))

    # setup vars
    result  = True

    # create a cursor
    cursor = db.cursor()

    # query
    query = "UPDATE bounce_aggr SET reported = now() WHERE mailbox = 'webmaster@trebnet.com' AND filename = %s AND recipient = %s"

    try:
        cursor.execute(query, (row['filename'], row['recipient']))
    except MySQLdb.IntegrityError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        result = False
    except MySQLdb.ProgrammingError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        result = False
    except MySQLdb.OperationalError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        result = False

    return result

def get_bounce_runs(db):
    logging.debug("get_bounce_runs")

    # setup vars
    result  = True

    # create a cursor
    cursor = db.cursor()

    # query
    query = "SELECT sender, subject, count(recipient) AS c FROM bounce_aggr WHERE mailbox = 'webmaster@trebnet.com' AND reported IS NULL GROUP BY sender, subject ORDER BY c DESC"

    try:
        cursor.execute(query, ())
    except MySQLdb.IntegrityError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        raise GetBounceRunsError, e
    except MySQLdb.ProgrammingError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        raise GetBounceRunsError, e
    except MySQLdb.OperationalError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        raise GetBounceRunsError, e

    for r in cursor:
        yield r

def get_recipients(db, sender, subject):
    logging.debug("get_recipients for run: [%s :: %s]" % (sender, subject))

    # setup vars
    result  = True

    # create a cursor
    cursor = db.cursor()

    # query
    query = "SELECT * FROM bounce_aggr WHERE mailbox = 'webmaster@trebnet.com' AND sender = %s AND subject = %s AND reported IS NULL ORDER BY recipient"

    try:
        cursor.execute(query, (sender, subject))
    except MySQLdb.IntegrityError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        raise GetRecipientsError, e
    except MySQLdb.ProgrammingError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        raise GetRecipientsError, e
    except MySQLdb.OperationalError, e:
        logging.error("error getting bounces [" + query + "]");
        logging.error(e)
        raise GetRecipientsError, e

    for r in cursor:
        yield r

def itemgetter(*items):
    if len(items) == 1:
        item = items[0]
        def g(obj):
            return obj[item]
    else:
        def g(obj):
            return tuple(obj[item] for item in items)
    return g

# Main
def main(dryrun):
    db_host = DB_HOST
    db_port = DB_PORT
    db_name = DB_NAME
    db_user = DB_USER
    db_pass = DB_PASS

    recipients = []

    # connect to database
    try:
        db = MySQLdb.connect(host=db_host, port=db_port, user=db_user, passwd=db_pass, db=db_name, cursorclass=MySQLdb.cursors.DictCursor)
    except MySQLdb.Error, e:
        logging.error("error connecting to [%s:%d:%s]: %s" % (db_host, db_port, db_name, e))
        return 1

    for run in get_bounce_runs(db):
        results = dict()
        recipients.append("%7d  %-40s  %s" % (run['c'], run['sender'], run['subject']))

        for r in get_recipients(db, run['sender'], run['subject']):
            if not r['recipient'] in results:
                results[r['recipient']] =  r['recipient_fullname']

            if dryrun is False:
                set_reported(db, r)

        list = ["%40s  %s" % ('Prospect Name', 'Email Address')]
        for email, fullname in sorted(results.iteritems(), key=itemgetter(1)):
            list.append("%40s  %s" % (fullname, email))

        text_body = text_message(list)
        html_body = html_message(list)

        if dryrun is False:
            logging.info("sending email to <%s> regarding [%s]" % (run['sender'], run['subject']))

            msg = MIMEMultipart('alternative')
            msg['Subject'] = DEFAULT_EMAIL_SUBJECT
            msg['From'] = DEFAULT_EMAIL_SENDER
            msg['To'] = run['sender']
            #msg['To'] = 'dferreira@basepair.org'
            #msg['To'] = 'iahmad@trebnet.com'
            #msg['To'] = 'swede.ewing@electricmail.com'

            part1 = MIMEText(text_body, 'plain')
            part2 = MIMEText(html_body, 'html')

            msg.attach(part1)
            msg.attach(part2)

            # send the message 
            smtp = smtplib.SMTP(DEFAULT_EMAIL_SERVER, DEFAULT_EMAIL_PORT)
            smtp.sendmail(DEFAULT_EMAIL_SENDER, run['sender'], msg.as_string())
            #smtp.sendmail(DEFAULT_EMAIL_SENDER, 'dferreira@basepair.org', msg.as_string())
            #smtp.sendmail(DEFAULT_EMAIL_SENDER, 'iahmad@trebnet.com', msg.as_string())
            #smtp.sendmail(DEFAULT_EMAIL_SENDER, 'swede.ewing@electricmail.com', msg.as_string())
            smtp.quit()
        else:
            print "To: %s" % run['sender']
            print "From: %s" % DEFAULT_EMAIL_SENDER
            print "Subject: %s" % DEFAULT_EMAIL_SUBJECT
            print ""
            print text_body
            print ""


    if dryrun is False:
        autodelete_reported(db)
        body = """
<b>%d</b> Reports sent out today.

<pre>
%s
%s
</pre>
""" % (len(recipients), "%-7s  %-40s  %s" % ('Bounces', 'Sender', 'Subject'), '\n'.join(recipients))

        msg = MIMEMultipart('alternative')
        msg['Subject'] = 'TREB Aggregated Bounce Reporter'
        msg['From'] = DEFAULT_EMAIL_SENDER
        msg['To'] = DEFAULT_REPORT_ADMIN
        part1 = MIMEText(body, 'html')
        msg.attach(part1)
        smtp = smtplib.SMTP(DEFAULT_EMAIL_SERVER, DEFAULT_EMAIL_PORT)
        smtp.sendmail(DEFAULT_EMAIL_SENDER, DEFAULT_REPORT_ADMIN, msg.as_string())
        smtp.quit()
        
    db.close()


if __name__ == '__main__':
    # vars
    dryrun = True

    # parse commandline options
    opts_short = "hDy"
    opts_long  = ['help', 'Debug', 'yes']

    try:
        opts, args = getopt.getopt(sys.argv[1:], opts_short, opts_long)
    except getopt.GetoptError, err:
        logging.error("error parsing commandline options: %s", err)
        usage()
        sys.exit(1)

    for o, a in opts:
        if o in ("-D", "--Debug"):
            logging.getLogger('').setLevel(logging.DEBUG)
        elif o in ("-h", "--help"):
            usage(sender, maildir, parts)
            sys.exit(1)
        elif o in ("-y", "--yes"):
            dryrun = False
        else:
            assert False, "unhandled option: [%s]" % a

    rc = main(dryrun)
    sys.exit(rc)
