#!/usr/bin/python

import cStringIO
import email
import logging
import logging.handlers
import imaplib
import re
import smtplib
import time

# python 2.4 vs 2.6
try:
        from email.mime.multipart import MIMEMultipart
except:
        from email.MIMEMultipart import MIMEMultipart

# python 2.4 vs 2.6
try:
        from email.mime.text import MIMEText
except:
        from email.MIMEText import MIMEText

SERVER    = 'inbound.electric.net'
PORT      = 993
SSL       = True
USERNAME  = 'trebinvoice@trebnet.com'
PASSWORD  = '[B@cfc089'

DEFAULT_EMAIL_SENDER        = 'TREBINVOICE Mail Admin <trebinvoice@trebnet.com>'
DEFAULT_EMAIL_SERVER        = 'outbound.electric.net'
DEFAULT_EMAIL_PORT          = 25
DEFAULT_EMAIL_SUBJECT       = 'Webmaster Mail Delivery Failure Report'
RECIPIENTS = []
#RECIPIENTS.append('dylan.ferreira@fusemail.com')
RECIPIENTS.append('invoicebounces@trebnet.com')

EMAIL_PATTERN  = re.compile('([\w\-\.]+@(\w[\w\-]+\.)+[\w\-]+)')

BLACKLIST = [ USERNAME ]

UNDELIVERABLES = []
UNDELIVERABLES.append('Undeliverable:')
UNDELIVERABLES.append('Undeliverable mail:')
UNDELIVERABLES.append('Message status - undeliverable')
UNDELIVERABLES.append('Undelivered Mail Returned to Sender')
UNDELIVERABLES.append('Delivery Final Failure Notice')
UNDELIVERABLES.append('DELIVERY FAILURE: ')
UNDELIVERABLES.append('Delivery Status Notification (Failure)')
UNDELIVERABLES.append('Mail delivery failed: returning message to sender')
UNDELIVERABLES.append('Returned mail: see transcript for details')
UNDELIVERABLES.append('Mail System Error - Returned Mail')
UNDELIVERABLES.append('Undeliverable Mail')
UNDELIVERABLES.append('Invalid e-mail address')
UNDELIVERABLES.append('Returned mail')
UNDELIVERABLES.append('Returned Mail')
UNDELIVERABLES.append('failure notice')
UNDELIVERABLES.append('Message Delivery Failure')
UNDELIVERABLES.append('Your Message Could Not Be Delivered')
UNDELIVERABLES.append('Delivery Status Notification(Failure)')
UNDELIVERABLES.append('Delivery Failure')

class EmailReport(object):
    def __init__(self, collection, email_sender=DEFAULT_EMAIL_SENDER, email_server=DEFAULT_EMAIL_SERVER, email_port=DEFAULT_EMAIL_PORT, email_subject=DEFAULT_EMAIL_SUBJECT, recipients=RECIPIENTS):
        self.collection     = collection
        self.email_sender   = email_sender
        self.email_server   = email_server
        self.email_port     = email_port
        self.email_subject  = email_subject
        self.recipients     = recipients

    def _indent(self, input):
        return "                %s" % input

    def format_collection(self):
        formatted = []

        for run in self.collection.runs.values():
            if run.start_date == run.end_date:
                date = '<tr><th align="right">Date:</th><td align="left"><i>%s</i></td></tr>\n' % (time.ctime(run.start_date))
            else:
                date = '<tr><th align="right">Dates:</th><td align="left">%d bounces from <i>%s</i> to <i>%s</i></td></tr>\n' % (len(run.recipients), time.ctime(run.start_date), time.ctime(run.end_date))
            recipients = '<tr><th align="right" valign="top">Recipients:</th><td align="left">%s</td></tr>\n' % ('<br />\n'.join(run.recipients))

            block = """<p>
    <h3>
        %s
    </h3>
    <table>
        %s
        %s
    </table>
</p>""" % (run.title, date, recipients)

            formatted.append(block)

        return "\n<hr />\n".join(formatted)

    def send(self):
        part1 = MIMEText("report in html", 'text')
        part2 = MIMEText(self.format_collection(), 'html')

        for r in self.recipients:
            logging.info("sending report to: [%s]" % r)
            msg = MIMEMultipart('alternative')
            msg['Subject'] = self.email_subject
            msg['From'] = self.email_sender
            msg['To'] = r

            msg.attach(part1)
            msg.attach(part2)

            smtp = smtplib.SMTP(self.email_server, self.email_port)
            smtp.sendmail(self.email_sender, r, msg.as_string())
            smtp.quit()

        return True

class BounceRun(object):
    def __init__(self, title):
        self.title      = title
        self.start_date = False
        self.end_date   = False
        self.recipients = []

class BounceRunCollection(object):
    def __init__(self):
        self.runs = dict()

    def run(self, title):
        if title not in self.runs:
            self.runs[title] = BounceRun(title)

        return self.runs[title]

class MessageData(object):
    def __init__(self, msg, email_pattern=EMAIL_PATTERN, undeliverables=UNDELIVERABLES, blacklist=BLACKLIST):
        self.msg            = msg
        self.email_pattern  = email_pattern
        self.undeliverables = undeliverables
        self.blacklist      = blacklist

        self.isgood         = False
        self.bounce         = False
        self.subject        = False
        self.to             = False

#        logging.error(self.msg['date'])
#        logging.error(email.utils.parsedate(self.msg['date']))

        try:
            self.time = time.mktime(email.utils.parsedate(self.msg['date']))
        except TypeError, e:
            logging.error("error parsing date: %s" % e)
            self.time = False

        self.is_bounce()
        self.parse_headers()

    def parse_headers(self):
        header_detected       = False
        orig_header_detected  = False

        fh = cStringIO.StringIO(self.msg.as_string())

        for l in fh.readlines():
            line = l.rstrip('\n')
            key, sep, data = line.partition(':')

            if header_detected is False:
                if len(line) < 1:
                    logging.debug("detected header at: [%s]" % line)
                    header_detected = True
                    continue
            elif orig_header_detected is False:
                if self.subject is False and key == 'Subject':
                    subject = data.lstrip(' ').rstrip(' ')
                    logging.debug("Found Original Subject: %s" % subject)
                    self.subject = subject

                if key == 'To':
                    try:
                        to = self.email_pattern.findall(line)[0][0].lower()
                    except:
                        logging.debug("address found in line [%s]" % line)
                        continue
                    logging.debug("Found Original To: %s" % to)
                    if to not in self.blacklist:
                        self.to = to

        fh.close()

        if self.subject is not False and self.to is not False and self.time > 0:
            self.isgood = True

        return True



    def is_bounce(self):
        # detect bounce
        for undeliverable in self.undeliverables:
            if 'subject' in self.msg:
                if self.msg['subject'].find(undeliverable) >= 0:
                    self.bounce = True
                    break

def main(server, port, username, password, ssl=False):
    # setup
    log_creds = "[%s:%d:%s]" % (server, port, username)
    messages  = dict()
    processed = []
    collection = BounceRunCollection()
    report     = EmailReport(collection)

    # connect and get message list
    try:
        if ssl is True:
            M = imaplib.IMAP4_SSL(server, port)
        else:
            M = imaplib.IMAP4(server, port)

        M.login(username, password)
        M.select()
        result, data = M.uid('search', None, "UNDELETED")
    except:
        raise AssertionError, "%s :: error connecting to imap server" % (log_creds)

    # loop through list
    for uid in data[0].split():
        # fetch message
        try:
            t, data = M.uid('fetch', uid, '(RFC822)')
        except:
            logging.error("%s [UID=%s] :: error fetching message" % (log_creds, uid))
            continue
    
        # check the response
        if t != "OK":
            logging.error("%s [UID=%s] :: error fetching message.  response = [%s]" % (log_creds, uid, t))
            continue

        # get the message data
        try:
            msg = email.message_from_string(data[0][1])
        except:
            logging.error("%s [UID=%s] :: error fetching message body" % (log_creds, uid))
            continue

        # setup
        md = MessageData(msg)

        if md.isgood is True and md.bounce is True:
            run = collection.run(md.subject)

            if run.start_date is False or md.time < run.start_date:
                run.start_date = md.time

            if run.end_date is False or md.time > run.end_date:
                run.end_date = md.time

            run.recipients.append(md.to)
    
        # this message is done, add the uid to the deletion list
        processed.append(uid)

    # send the report
    report.send()

    # mark the processed messages as deleted
    if len(processed) > 0:
        deletion_list = ', '.join(processed)
        logging.info("%s :: marking messages as deleted: [%s]" % (log_creds, deletion_list))

    for uid in processed:

        try:
            M.uid('STORE', uid, '+FLAGS', '(\Deleted)')
        except:
            logging.warning("%s :: error marking message as deleted: [%s]" % (log_creds, uid))

    # expunge and logout
    print M.expunge()
    M.close()
    M.logout()

if __name__ == '__main__':
    import sys

    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s :: %(message)s')
    logging.getLogger('').setLevel(logging.DEBUG)

    try:
        main(SERVER, PORT, USERNAME, PASSWORD, ssl=SSL)
    except AssertionError, e:
        logging.error(e)
        sys.exit(1)
