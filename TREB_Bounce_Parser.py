#!/usr/bin/env python

import getopt
import gzip
import logging
import logging.handlers
import MySQLdb
import MySQLdb.cursors
import os
import os.path
import quopri
import re
import sys
import time
import pprint

# Vars
DB_HOST = 'dbo.local'
DB_PORT = 3306
DB_USER = 'system'
DB_PASS = 'tjmwauki'
DB_NAME = 'bounce_aggr'
PARTS   = ['cur']

# Setup
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# Classes
class Mailbox(object):
    def __init__(self, box_fulladdress, dbhost='aka.local', dbport=3306, dbuser='system', dbpass='tjmwauki', dbname='akabeans'):
        self.box_fulladdress = box_fulladdress
        self.dbhost          = dbhost
        self.dbport          = dbport
        self.dbuser          = dbuser
        self.dbpass          = dbpass
        self.dbname          = dbname
        self.data            = False

    def __getitem__(self, key):
        if self.data is False:
            self._get_mailboxes_data()

        if key in self.data:
            return self.data[key]

        return False

    def _get_mailboxes_data(self):
        box_id, aka_domain = self.box_fulladdress.split('@')

        akabeans = MySQLdb.connect(host=self.dbhost, port=self.dbport, user=self.dbuser, passwd=self.dbpass, db=self.dbname, cursorclass=MySQLdb.cursors.DictCursor)
        cursor = akabeans.cursor()
        query = "SELECT * FROM mailboxes WHERE aka_domain = %s AND box_fulladdress = %s"
        cursor.execute(query, (aka_domain, self.box_fulladdress))

        self.data = cursor.fetchone()

        cursor.close()
        akabeans.close()

        return True
        

class EMCUser(object):
    def __init__(self, box_fulladdress):
        self.box_fulladdress  = box_fulladdress
        self.mailbox_data     = Mailbox(box_fulladdress)

    def ccd(self):
        # billing@annetteashley.com
        # com/a/annetteashley/users/b/i/billing

        box_id, aka_domain = self.box_fulladdress.split('@')
        domain_parts = aka_domain.split('.')
        path = [ self.mailbox_data['box_maildir'] ]


        tld = domain_parts.pop()
        path.append(tld)

        for p in reversed(domain_parts):
            path.append(p[0])
            path.append(p)

        path.append('users')

        mbox_id = []
        i = 0
        for c in box_id:
            if c == '/':
                c = '_'

            mbox_id.append(c)

            if i < 2:
                if c == '.':
                    c = 'q'

                path.append(c)

            i += 1

        if  i < 2:
            path.append('@')

        path.append(''.join(mbox_id))

        return '/'.join(path)

class BounceMessage(object):
    def __init__(self, filename):
        self.filename           = filename

        self.stat = os.stat(self.filename)

        self.received_time      = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(self.stat[8]))
        self.date               = False
        self.recipient          = False
        self.orig_date          = False
        self.orig_from          = False
        self.orig_replyto       = False
        self.orig_subject       = False
        self.full_name          = False

        self.isgood             = False

        self.parse()

    def parse(self):
        header_detected       = False
        orig_header_detected  = False
        body = []
    
        email_pattern      = re.compile('([\w\-\.]+@(\w[\w\-]+\.)+[\w\-]+)')
        #full_name_pattern  = re.compile('Property Match Unsubscribe To \'(.+?)\'&')
    
 #       full_name_pattern  = re.compile('unsubscribe.*click.*email\s*(.+?),',re.MULTILINE)
        full_name_pattern  = re.compile('atches\s+for:\s*(.+?)\s*[0-9]',re.MULTILINE)

        #full_name_pattern  = re.compile('To unsubscribe from this service, click here to email (.+?),')
        #fh = open(self.filename, 'r')
        fh = gzip.GzipFile(self.filename, "r")

        for l in fh.readlines():
            line = l.rstrip('\n')
  #          print "O: ",l
            key, sep, data = line.partition(':')
    
            if header_detected is False:
                if len(line) < 1:
                    logging.debug("detected header at: [%s]" % line)
                    header_detected = True
                    continue
    
                if key == 'X-Failed-Recipients':
                    r = email_pattern.findall(data)
                    if len(r) > 0:
                        self.recipient = r[0][0].lower()
                        logging.debug("Found X-Failed-Recipients address <%s>" % self.recipient)
   
                if key == 'Date':
                    date = data.lstrip(' ').rstrip(' ')
                    logging.debug("Found Date: %s" % date)
                    self.date = date
            elif orig_header_detected is False:
                if self.orig_from is False and key == 'From':
                    if email_pattern.findall(line):
                        orig_from = email_pattern.findall(line)[0][0].lower()
                        logging.debug("Found Original From: <%s>" % orig_from)
                        self.orig_from = orig_from
    
                if self.orig_replyto is False and key == 'Reply-To':
                    if email_pattern.findall(line):
                        orig_replyto = email_pattern.findall(line)[0][0].lower()
                        logging.debug("Found Original Reply-To: <%s>" % orig_replyto)
                        self.orig_replyto = orig_replyto
    
                if self.orig_subject is False and key == 'Subject':
                    orig_subject = data.lstrip(' ').rstrip(' ')
                    logging.debug("Found Original Subject: %s" % orig_subject)
                    self.orig_subject = orig_subject

                if self.orig_date is False and key == 'Date':
                    orig_date = data.lstrip(' ').rstrip(' ')
                    logging.debug("Found Original Date: %s" % orig_date)
                    self.orig_date = orig_date
    
                if (self.orig_from is not False and self.orig_replyto is not False and self.orig_subject is not False and self.orig_date is not False and self.date is not False):
                    logging.debug("Detected original header at: [%s]" % line)
                    orig_header_detected = True
                    continue
            else:
                body.append(l)

        self.body = quopri.decodestring(''.join(body))

        names = full_name_pattern.findall(self.body)

        if len(names) > 0:
            #self.full_name = re.sub(r'\s+home\s+(src|srch|search)$', '', names[0], re.IGNORECASE)
            self.full_name = names[0]
            logging.debug("detected recipient full_name: [%s]" % self.full_name)
        else:
            logging.error("full_name not found for: %s" % self.filename)

        if self.full_name is not False and self.recipient is not False and self.orig_replyto is not False:
            self.isgood = True

        fh.close()

    def delete(self):
        logging.debug("DELETING FILE: %s" % self.filename)
        try:
            os.unlink(self.filename)
        except:
            return False

        return True
    
# Subs
def usage(sender, maildir, parts):

    print """
Usage: %s --script=<scriptname> --maxlocks=<int>]

    -s, --sender=<email>               : filter by this sending email address (default: %s)
    -m, --maildir=<path>               : path to maildir (default: %s)
    -p, --parts=<parts>                : cur, new, tmp, etc.. (default: new)
    -D, --Debug                        : enable Debug logging.
    -h, --help                         : this text.
    """ % (sys.argv[0], sender, maildir)

    return True

def store_bounce(db, user, msg):
    # setup vars
    result  = True

    # create a cursor
    cursor = db.cursor()

    # query
    query = ("INSERT IGNORE INTO bounce_aggr "
             "(mailbox, filename, received, sender, recipient, recipient_fullname, sent, subject) "
             "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
         )

#    print query
    s= ":*:"
    # logging.debug(query, s.join ([ user.box_fulladdress,
    #                       msg.filename,
    #                       msg.received_time,
    #                       msg.orig_replyto,
    #                       msg.recipient,
    #                       msg.full_name,
    #                       msg.orig_date,
    #                                msg.orig_subject]))
#    pp = pprint.PrettyPrinter(indent=2)
#    pp.pprint( msg.filename)
#    pp.pprint(  [query, user.box_fulladdress, msg.filename, msg.received_time, msg.orig_replyto,  msg.recipient,  msg.full_name, msg.orig_date,  msg.orig_subject] )
#    pp.pprint(db)
    try:
        cursor.execute(query, (user.box_fulladdress,
                               msg.filename,
                               msg.received_time,
                               msg.orig_replyto,
                               msg.recipient,
                               msg.full_name,
                               msg.orig_date,
                               msg.orig_subject))
        logging.debug(cursor._last_executed)
        db.commit()

    except MySQLdb.IntegrityError, e:
        logging.error("error adding bounce entry [" + query + "]");
        logging.error(e)
        raise AddRecordError, e
    except MySQLdb.ProgrammingError, e:
        logging.error("error adding bounce entry [" + query + "]");
        logging.error(e)
        raise AddRecordError, e
    except MySQLdb.OperationalError, e:
        logging.error("error adding bounce entry [" + query + "]");
        logging.error(e)
        raise AddRecordError, e

    return result

def get_messages(path):
    logging.debug("scanning path: [%s]" % path)
    for f in os.listdir(path):
        if f.startswith('.'):
            continue
        yield f

# Main
def main(user, sender, maildir, parts, db_host=DB_HOST, db_port=DB_PORT, db_user=DB_USER, db_pass=DB_PASS, db_name=DB_NAME):
    # connect to database
    try:
        dbo = MySQLdb.connect(host=db_host,
                             port=db_port,
                             user=db_user,
                             passwd=db_pass,
                             db=db_name)
#                             cursorclass=MySQLdb.cursors.DictCursor)
    except MySQLdb.Error, e:
        logging.error("error connecting to [%s:%d:%s]: %s" % (db_host, db_port, db_name, e))
        return 1

    for p in parts:
        path = os.path.join(maildir, p)

        if not os.path.exists(path):
            logging.error("skipping non-existent path: %s" % path)
            continue

        for filename in get_messages(path):
            full_filename = os.path.join(maildir, p, filename)
            msg = BounceMessage(full_filename)

            print "store g=",msg.isgood, " msg.o=>",msg.orig_from, "< send=>",sender,"<"
            if msg.isgood is True and msg.orig_from.strip() == sender.strip():

                store_bounce(dbo, user, msg)
                print "%s    <%s>  [%s]   \"%s\" <%s>   [%s]" % (msg.received_time, msg.orig_replyto, msg.orig_subject, msg.full_name, msg.recipient, msg.orig_date)

            msg.delete()

    dbo.close()


if __name__ == '__main__':
    # vars
    parts   = PARTS
    sender  = False

    # parse commandline options
    opts_short = "hDs:p:m:"
    opts_long  = ['help', 'Debug', 'sender=', 'path=', 'maildir=']

    try:
        opts, args = getopt.getopt(sys.argv[1:], opts_short, opts_long)
    except getopt.GetoptError, err:
        logging.error("error parsing commandline options: %s", err)
        usage()
        sys.exit(1)

    for o, a in opts:
        if o in ("-D", "--Debug"):
            logging.getLogger('').setLevel(logging.DEBUG)
        elif o in ("-h", "--help"):
            usage(sender, maildir, parts)
            sys.exit(1)
        elif o in ("-s", "--sender"):
            sender = a
        elif o in ("-p", "--parts"):
            parts.append(a)
        else:
            assert False, "unhandled option: [%s]" % a

    if sender is False:
        logging.error("require the senders email address (-s <sender>)")
        sys.exit(1)

    user = EMCUser(sender)
 
    maildir = "%s/Maildir" % user.ccd()

#    maildir = "testdata"

#    print "rc = main(%s, %s, %s, %s)" % (user, sender, maildir, parts)
#    sys.exit(1)

    rc = main(user, sender, maildir, parts)
    sys.exit(rc)
